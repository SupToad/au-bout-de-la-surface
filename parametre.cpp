#include "parametre.h"

Parametre::Parametre()
{}

Parametre::Parametre(unsigned nb, float* param)
{
    for (unsigned i = 0 ; i < nb ; ++i)
        addParamf(param[i]);
}

Parametre::Parametre(unsigned nb, QVector<Point>* param)
{
    for (unsigned i = 0 ; i < nb ; ++i)
        paramp.append(param[i]);
}

void Parametre::addParamf(const float param)
{
    paramf.append(param);
}

void Parametre::addParamp(const Point param, unsigned int to)
{
    paramp.data()[to].append(param);
}

