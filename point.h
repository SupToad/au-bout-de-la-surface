/* R. Raffin
 * M1 Informatique, Aix-Marseille Université
 * définition d'une classe Point
 * l'utilisation du tableau de float pour les coordonnées est là pour ne pas trop avoir peur de C++
 * mais c'(est une mauvaise façon d'écrire, préférez les objets de std::, comme std::vector par exemple
 * Cet objet est d'ailleurs directement utilsable comme un float[]
 * Si on veut généraliser le Point, il peut être d'une quelconque dimension (i.e. un nombre quelconque de coordonnées)
 */

#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <QDebug>
#include <string>
#include <math.h>

/*!
 * \brief Classe représentant un point
 */
class Point
{
private:
    /*!
     * \brief coords: Tableau de coordonnées des points de taille dimension
     */
	float * coords;

    /*!
     * \brief dimension: Dimension du point et taille du tableau de coordonnées
     */
    unsigned dimension;

public:
    /*!
     * \brief Constructeur par défaut de la classe Point
     */
	Point();

    /*!
     * \brief Constructeur de la classe point à partir d'un tableau de coordonnées
     */
    Point(unsigned, float *);

    /*!
     * \brief Constructeur de la classe point à partir de 3 coordonnées
     * \param x: Coordonnées x du Point
     * \param y: Coordonnées y du Point
     * \param z: Coordonnées z du Point
     */
    Point(float x, float y, float z);

    /*!
     * \brief Destructeur par défaut de la classe Point
     */
	~Point();

    /*!
     * \brief Constructeur par recopie de la classe Point
     */
	Point(const Point&);

    /*!
     * \brief Calcul de la norme du point
     * \return Retourne la norme
     */
    float norm();

    /*!
     * \brief getter de la variable dimension
     */
    unsigned getDimension() const;

    /*!
     * \brief getter de la coordonnées à l'index N du tableau de coordonnées
     * \return Retourne la coordonnées
     */
	float getN(unsigned) const;

    /*!
     * \brief getter de la coordonnée x
     * \return Retourne la coordonnée à l'index 0 du tableau de coordonnées
     */
	float getX() const;

    /*!
     * \brief getter de la coordonnée y
     * \return Retourne la coordonnée à l'index 1 du tableau de coordonnées
     */
	float getY() const;

    /*!
     * \brief getter de la coordonnée z
     * \return Retourne la coordonnée à l'index 2 du tableau de coordonnées
     */
	float getZ() const ;

    /*!
     * \brief getter du tableau de coordonnées
     * \return Retourne le tableaud de coordonnées
     */
	void get(float *) const;


    /*!
     * \brief Setter de la variable dimension
     */
    void setDimension(unsigned);

    /*!
     * \brief setter de la coordonnées à l'index N du tableau de coordonnées
     */
	void setN(unsigned, const float&);

    /*!
     * \brief setter de la coordonnées à l'index 0 du tableau de coordonnées
     */
	void setX(const float&);

    /*!
     * \brief setter de la coordonnées à l'index 1 du tableau de coordonnées
     */
	void setY(const float&);

    /*!
     * \brief setter de la coordonnées à l'index 2 du tableau de coordonnées
     */
	void setZ(const float&);

    /*!
     * \brief setter du tableau de coordonnées
     */
    void set(const float *);

    /*!
     * \brief Surchage de l'opérateur d'affectation
     * \return Le nouveau point
     */
    Point& operator= (const Point &);

    /*!
     * \brief Surcharge de l'opérateur d'injection de flux
     * \return Retourne un OutputStream
     */
    friend std::ostream& operator<<(std::ostream&, const Point&);

    /*!
     * \brief Surcharge de l'opérateur d'addition
     * \param p1: Premier point à additionner
     * \param p2: Second point à additionner
     * \return Retourne le Point issu du calcul
     */
    friend Point operator+(const Point& p1, const Point& p2);

    /*!
     * \brief Surcharge de l'opérateur de soustraction
     * \param p1: Point originel
     * \param p2: Point à soustraire à p1
     * \return Retourne le Point issu du calcul
     */
    friend Point operator-(const Point& p1, const Point& p2);

    /*!
     * \brief Surcharge de l'opérateur de multiplication
     * \param f: réel à multiplier aux coordonnées du point
     * \param p: Point à multiplier
     * \return Retourne le point issu du calcul
     */
    friend Point operator*(const float& f, const Point& p);

    /*!
     * \brief Surcharge de l'opérateur |, que l'on affecte au produit scalaire
     * entre deux points
     * \param p1
     * \param p2
     * \return Retourne le produit scalaire des points p1 et p2
     */
    friend float operator|(const Point& p1, const Point& p2);
};

#endif // POINT_H
