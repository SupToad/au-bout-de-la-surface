#include "discretisation.h"

Discretisation::Discretisation()
{
}

QVector<Point> Discretisation::discretiserSegment(QVector<Segment> seg_list, Parametre p)
{
    QVector<float> param_list = p.getParamf();
    QVector<Point> point_list;
    for (auto const& s:seg_list)
        for (auto const& value:param_list)
            point_list.append(s.getValue(value));
    return point_list;
}

QVector<Point> Discretisation::discretiserSurface(QVector<CourbeParam*> cur_list, float delta_u, float delta_v)
{
    QVector<Point> point_list;
    // Pour toutes les courbes passées
    for (auto const& c:cur_list)
    {
        // En avançant par pas homogène,
        // On récupère les points par 4
        // pour construire la surface.
        for (float v = 0 ; v <= 1 - delta_v+0.001 ; v+=delta_v)
            for (float u = 0 ; u <= 1 - delta_u+0.001 ; u+=delta_u)
            {
                float f0[2] = {u,v};
                point_list.append(c->getValue(Parametre(2,f0)));

                float f1[2] = {u ,v + delta_v};
                point_list.append(c->getValue(Parametre(2,f1)));

                float f3[2] = {u + delta_u , v};
                point_list.append(c->getValue(Parametre(2,f3)));

                float f2[2] = {u + delta_u ,v + delta_v};
                point_list.append(c->getValue(Parametre(2,f2)));
            }
    }

    return point_list;
}

QVector<Point>  Discretisation::avancee2Front (QVector<CourbeParam*> cur_list, float delta, double seuil_bas, double seuil_haut)
{
    QVector<Point> point_list;
    float epsilon = 0.0001f;

    // Pour toutes les courbes passées
    for (auto const& c:cur_list)
    {
        // Récupère son premier point
        float delta_t = delta;
        point_list.append(c->getValue(0));

        float t = 0.0f;
        while ((t+delta_t) < 1)
        {
            // Si delta_t ne peut plus diminuer,
            // on arrête ici pour ne pas être
            // dans une boucle infinie
            if (delta_t/2.0f == 0.0f)
            {
                qDebug() << "Issue : delta_t null : " << delta_t << " (t =" << t << ")";
                exit(EXIT_FAILURE);
            }

            Point p1 = c->getVector(t,t+epsilon);
            Point p2 = c->getVector(t+delta_t,t+delta_t+epsilon);
            float dp = (p1|p2)/(p1.norm()*p2.norm());

            // En fonction de la valeur, on
            // augmente ou diminue le pas ou
            // on ajoute un point
            if (abs(acos(dp)) > seuil_haut)
                delta_t /= 2;
            else
            {
                if (abs(acos(dp)) < seuil_bas)
                    delta_t += delta_t/2;
                else
                {
                    point_list.append(c->getValue(t+delta_t));
                    t +=delta_t;
                }
            }
        }
        // On récupère le dernier point de la courbe
        point_list.append(c->getValue(1));
    }
    return point_list;
}
