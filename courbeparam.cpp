#include "courbeparam.h"

CourbeParam::CourbeParam()
{
    vertices = nullptr;
}

CourbeParam::~CourbeParam()
{
    delete [] vertices;
}


Point CourbeParam::getVector(float param1, float param2) const
{
    return getValue(param2)-getValue(param1);
}

Point CourbeParam::getVector(Parametre param1, Parametre param2) const
{
    return getValue(param2)-getValue(param1);
}

void CourbeParam::setN(unsigned r, const Point & p)
{
    if (pointList.isEmpty() || r > getSize() - 1)
        pointList.append(p);
    else
        pointList.data()[r] = p;
}

Point CourbeParam::getN(unsigned r) const
{
    if (r > getSize() - 1)
        r = getSize() - 1;

    return pointList.data()[r];
}




