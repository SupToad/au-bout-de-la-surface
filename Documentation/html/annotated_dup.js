var annotated_dup =
[
    [ "Bezier", "class_bezier.html", "class_bezier" ],
    [ "CarreauBezier", "class_carreau_bezier.html", "class_carreau_bezier" ],
    [ "CourbeParam", "class_courbe_param.html", "class_courbe_param" ],
    [ "Discretisation", "class_discretisation.html", "class_discretisation" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "myOpenGLWidget", "classmy_open_g_l_widget.html", "classmy_open_g_l_widget" ],
    [ "MyTraits", "struct_my_traits.html", "struct_my_traits" ],
    [ "Parametre", "class_parametre.html", "class_parametre" ],
    [ "Point", "class_point.html", "class_point" ],
    [ "Segment", "class_segment.html", "class_segment" ]
];