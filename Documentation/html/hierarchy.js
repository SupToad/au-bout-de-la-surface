var hierarchy =
[
    [ "CourbeParam", "class_courbe_param.html", [
      [ "Bezier", "class_bezier.html", [
        [ "CarreauBezier", "class_carreau_bezier.html", null ]
      ] ],
      [ "Segment", "class_segment.html", null ]
    ] ],
    [ "DefaultTraits", null, [
      [ "MyTraits", "struct_my_traits.html", null ]
    ] ],
    [ "Discretisation", "class_discretisation.html", null ],
    [ "MainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "Parametre", "class_parametre.html", null ],
    [ "Point", "class_point.html", null ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QOpenGLFunctions", null, [
      [ "myOpenGLWidget", "classmy_open_g_l_widget.html", null ]
    ] ],
    [ "QOpenGLWidget", null, [
      [ "myOpenGLWidget", "classmy_open_g_l_widget.html", null ]
    ] ]
];