var struct_my_traits =
[
    [ "EdgeAttributes", "struct_my_traits.html#aea0c7ea4a3b8466349a3d5ef15287dbf", null ],
    [ "FaceAttributes", "struct_my_traits.html#a3a25f0db783d8efcd8dddd0b8cea325a", null ],
    [ "HalfedgeAttributes", "struct_my_traits.html#aade3f1e764750e0eac8008a814b98ae1", null ],
    [ "VertexAttributes", "struct_my_traits.html#a37dbc046a6c1a720a97f3c4bc84d99fa", null ],
    [ "EdgeTraits", "struct_my_traits.html#a77586508c7334a556e57a2b189feb11a", null ],
    [ "VertexTraits", "struct_my_traits.html#ac77671eff45753af5f4fd2d456de9286", null ]
];