#include "point.h"

Point::Point()
{
    dimension = 3;
    coords = new float[3];
    coords[0] = coords[1] = coords[2] = 0;
}

Point::Point (unsigned dim, float * coordinates) {
    if ( dim != sizeof ( coordinates ) / sizeof ( float ) ) {
        qDebug ( ) << "Erreur: La dimension fournie ne correspond pas au nombre de coordonnées passées.";
    }

    dimension = dim;
    coords = new float[dimension];
    for ( unsigned i = 0 ; i < dimension ; i++ )
        coords[i] = coordinates[i];
}

Point::Point(float x, float y, float z)
{
    dimension = 3;
    coords = new float[3];
    coords[0] = x;
    coords[1] = y;
    coords[2] = z;
}

Point::~Point()
{
	delete [] coords;
	coords = nullptr;
}

Point::Point(const Point& p)
{
    dimension = p.dimension;
    coords = new float[dimension];
    for (unsigned i=0; i<dimension; ++i)
		coords[i] = p.coords[i];
}

unsigned Point::getDimension ( ) const {
    return dimension;
}

float Point::getX() const
{
	return getN(0);
}

float Point::getY() const
{
	return getN(1);
}

float Point::getZ() const
{
	return getN(2);
}

void Point::get(float * t) const
{
    for (unsigned i = 0 ; i < dimension ; ++i)
        t[i] = coords[i];
}

float Point::getN(unsigned r) const
{
    if ( r > dimension )
        r = dimension;
	return coords[r] ;
}

void Point::setDimension(unsigned dim) {
    dimension = dim;
}

void Point::setN(unsigned r, const float & v)
{
    if ( r > dimension )
        r = dimension;
    coords[r] = v;
}

void Point::set(const float * t)
{
    for ( unsigned i = 0 ; i < dimension ; ++i)
        coords[i] = t[i];
}

void Point::setX(const float& v)
{
	setN(0, v);
}
void Point::setY(const float& v)
{
	setN(1, v);
}
void Point::setZ(const float& v)
{
	setN(2, v);
}

float Point::norm()
{
    float res = 0.0f;
    for (unsigned i = 0 ; i < getDimension() ; ++i)
        res += pow(getN(i),2);
    return sqrt(res);
}

Point& Point::operator= (const Point &p)
{
    dimension = p.dimension;
    for ( unsigned i = 0 ; i < dimension ; ++i)
        coords[i] = p.coords[i];

    return *this;
}

Point operator+(const Point& p1, const Point& p2)
{
    Point r;
    if(p1.dimension != p2.dimension) {
        qDebug ( ) << "Opération impossible la dimension de p1 est différente de celle de p2";
    }
    for ( int i = 0 ; i < p1.dimension ; ++i )
        r.setN ( i , p1.getN ( i ) + p2.getN ( i ) );
    return r;
}

Point operator-(const Point& p1, const Point& p2)
{
    Point r;
    if(p1.dimension != p2.dimension) {
        qDebug ( ) << "Opération impossible la dimension de p1 est différente de celle de p2";
    }
    for ( int i = 0 ; i < p1.dimension ; ++i )
        r.setN ( i , p1.getN ( i ) - p2.getN ( i ) );
    return r;
}

Point operator*(const float& f, const Point& p)
{
    Point r;
    for ( int i = 0 ; i < p.dimension ; ++i )
        r.setN ( i , f * p.getN ( i ) );
    return r;
}


float operator|(const Point& p1, const Point& p2)
{
    float r = 0.0f;
    if (p1.dimension != p2.dimension)
        qDebug ( ) << "Opération impossible la dimension de p1 est différente de celle de p2";
    for ( int i = 0 ; i < p1.dimension ; ++i )
        r += p1.getN(i)*p2.getN(i);
    return r;
}

std::ostream& operator<<(std::ostream& out, const Point& p)
{
    out << "[ ";
    for ( unsigned i = 0 ; i < p.dimension ; ++i ) {
        out << std::to_string ( p.coords[i] ) << " ";
    }
    out << "]";

    return out;
}
