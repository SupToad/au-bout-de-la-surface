#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "./OpenMesh/inc/OpenMesh/Core/IO/MeshIO.hh"
#include "./OpenMesh/inc/OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh"
#include "ui_mainwindow.h"

using namespace OpenMesh;
using namespace OpenMesh::Attributes;


/*!
 * \brief Structure stockant les propriétés d'un mesh
 */
struct MyTraits : public OpenMesh::DefaultTraits
{
    // use vertex normals and vertex colors
    VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
    // store the previous halfedge
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
    // use face normals face colors
    FaceAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
    EdgeAttributes( OpenMesh::Attributes::Color );
    // vertex thickness
    VertexTraits{float thickness;};
    // edge thickness
    EdgeTraits{float thickness;};
};
typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;

/*!
 * \brief Classe d'interface principale de l'application
 */
class MainWindow : public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
    /*!
     * \brief Constructeur de la classe MainWindow
     * \param parent: Nécéssaire, néamoins la classe ne possède pas de parent
     */
    explicit MainWindow(QWidget *parent = nullptr);

    /*!
     * \brief Récupère la valeur de la spinbox u_value depuis l'IHM
     * \return La valeur de u_value
     */
    double getUValue();

    /*!
     * \brief Récupère la valeur de la spinbox v_value depuis l'IHM
     * \return La valeur de v_value
     */
    double getVValue();

    /*!
     * \brief Récupère la valeur de la spinbox step_value depuis l'IHM
     * \return La valeur de step_value
     */
    double getStepValue();

private:

    MyMesh mesh;
    Ui::MainWindow *ui;


private slots:

    /*!
     * \brief Action à effectuer quand on clique sur le bouton "Chargement d'un
     * polyèdre de contrôle"
     */
    void on_pushButton_chargement_clicked ( );

    /*!
     * \brief Action à effectuer quand la valeur de la spinbox u_value,
     * permettant de modifier l'emplacement du point mobile, est changée.
     * La nouvelle valeur est envoyée à la drawArea qui redessine la scène en fonction
     * de ce nouveau paramètre.
     * \param arg1: Nouvelle valeur
     */
    void on_u_value_valueChanged(double arg1);

    /*!
     * \brief Action à effectuer quand la valeur de la spinbox v_value,
     * permettant de modifier l'emplacement du point mobile, est changée.
     * La nouvelle valeur est envoyée à la drawArea qui redessine la scène en fonction
     * de ce nouveau paramètre.
     * \param arg1: Nouvelle valeur
     */
    void on_v_value_valueChanged(double arg1);

    /*!
     * \brief Action à effectuer quand la valeur de la spinbox step_value,
     * permettant de changer le pas de discrétisation, est changée.
     * La nouvelle valeur est envoyée à la drawArea qui redessine la scène en fonction
     * de ce nouveau paramètre.
     * \param arg1: Nouvelle valeur
     */
    void on_step_value_valueChanged(double arg1);

    /*!
     * \brief Action à effectuer lorsque que le bouton "Forme filaire/Forme pleine"
     * est cliqué. Cela demande à la drawArea de dessiner la scène avec le mode de dessin
     * non-utilisé actuellement.
     */
    void on_shape_clicked();

    /*!
     * \brief Action à effectuer quand la valeur du slider x_angle est modifiée.
     * La nouvelle valeur d'angle est envoyée à la drawArea, qui redessine la scène
     * avec une rotation de valeur value autour de l'axe x.
     * \param value: Nouvelle valeur
     */
    void on_x_angle_valueChanged(int value);

    /*!
     * \brief Action à effectuer quand la valeur du slider y_angle est modifiée.
     * La nouvelle valeur d'angle est envoyée à la drawArea, qui redessine la scène
     * avec une rotation de valeur value autour de l'axe y.
     * \param value: Nouvelle valeur
     */
    void on_y_angle_valueChanged(int value);
};

#endif // MAINWINDOW_H
