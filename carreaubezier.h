#ifndef CARREAUBEZIER_H
#define CARREAUBEZIER_H

#include "bezier.h"

/*!
 * \brief Classe représentant un carreau de Béziers, héritant de la classe Béziers
 */
class CarreauBezier : public Bezier
{
private:
    /*!
     * \brief Nombre de points de contrôles
     */
    unsigned size;
public:
    /*!
     * \brief Constructeur prenant en paramètre deux dimensions m et n et un tableau
     * C de Point de taille m * n
     * \param m: Dimension
     * \param n: Dimension
     * \param points: Tableaux des points de contrôles
     */
    CarreauBezier(unsigned m, unsigned n, Point* points);

    /*!
     * \brief Constructeur prenant en paramètre deux dimensions m et n et un QVector
     * de Point de taille m * n
     * \param m: Dimension
     * \param n: Dimension
     * \param points: Tableaux des points de contrôles
     */
    CarreauBezier(unsigned m, unsigned n, QVector<Point> points);

    ~CarreauBezier();

    /*!
     * \brief Calcule les coordonnées d'une Point sur le carreau de Béziers à partir
     * d'une instance de la classe Paramètre, contenant plusieurs paramètres or-
     * donnés à utiliser dans le polynome de Bernstein de la courbe
     * \param param: Instance de la classe Param
     * \return Retourne le Point calculé.
     */
    Point getValue(Parametre param) const;
};

#endif // CARREAUBEZIER_H
