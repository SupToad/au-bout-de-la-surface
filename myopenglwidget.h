#ifndef MYOPENGLWIDGET_H
#define MYOPENGLWIDGET_H

#define GL_GLEXT_PROTOTYPES
#include <QDebug>
#include "QMouseEvent"
#include <OpenMesh/Core/Geometry/VectorT.hh>

#include <QObject>
#include <QWidget>

#include <QKeyEvent>
#include <QTimer>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QGLWidget>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>
#include <GL/glext.h>
#include <GL/glut.h>

#include "segment.h"
#include "bezier.h"
#include "carreaubezier.h"
#include "discretisation.h"


/*!
 * \brief Classe représentant la zone d'affichage OpenGL
 */
class myOpenGLWidget : public QOpenGLWidget,
			   protected QOpenGLFunctions
{
	Q_OBJECT

public:
    /*!
     * \brief Getter de la variable u
     * \return Valeur de la variable u
     */
    double getU () const;

    /*!
     * \brief Getter de la variable v
     * \return Valeur de la variable v
     */
    double getV () const;

    /*!
     * \brief Getter de la variable step
     * \return Valeur de la variable step
     */
    double getStep () const;

    /*!
     * \brief Getter de la forme à l'index param du tableau de formes
     * \param param: Index de la forme à retourner
     * \return La form voulue
     */
    inline CourbeParam* getForme(unsigned param) const {return formes.at(param);}

    /*!
     * \brief Getter de la variable nb_param_m
     */
    inline unsigned getNbParamM () const {return nb_param_m;}

    /*!
     * \brief Getter de la variable nb_param_n
     */
    inline unsigned getNbParamN () const {return nb_param_n;}

    /*!
     * \brief Renvoie le nombre de paramètre total
     */
    inline unsigned getNbParam () const {return nb_param_m*nb_param_n;}

    /*!
     * \brief Setter de la variable u
     */
    void setU ( double );

    /*!
     * \brief Setter de la variable v
     */
    void setV ( double );

    /*!
     * \brief Setter de la variable steo
     */
    void setStep ( double );

    /*!
     * \brief getter du booléen fill
     * \return valeur du booléen fill
     */
    bool getFill ();

    /*!
     * \brief Inverse la valeur du booléen fill
     */
    void switchFill ();

    /*!
     * \brief Getter de la variable angle_y
     * \return
     */
    double getAngle_y ();

    /*!
     * \brief Setter de la variable angle_y<
     * \param angle_y: Nouvelle valeur
     */
    void setAngle_y ( double angle_y );

    /*!
     * \brief Getter de la variable angle_x
     * \return
     */
    double getAngle_x ();

    /*!
     * \brief Setter de la varaible angle_x
     * \param angle_x: Nouvelle valeur
     */
    void setAngle_x ( double angle_x );

	explicit myOpenGLWidget(QWidget *parent = nullptr);
	~myOpenGLWidget();

    /*!
     * \brief Fonction générant le tableau de couleur à utiliser pour
     * l'affichage des points
     * \param nb_points: Nombre de points à colorer
     */
    void generateColors(unsigned nb_points);

    /*!
     * \brief getter du tableau de couleurs
     * \return
     */
    inline float* getColors() const {return colors;}

    /*!
     * \brief Buffer pour les VBO
     */
    GLuint TriDataBuffers[2];

    int triToDraw;

    GLuint LinesDataBuffers[2];
    int linesToDraw;
    QList<QPair<float, int> > edgeSizes;

    GLuint PointsDataBuffers[2];
    int pointsToDraw;
    QList<QPair<float, int> > vertsSizes;
    QVector<GLfloat> vert_data;

    /*!
     * \brief Permet de savoir si les buffers sont initialisés
     */
    bool init;

    // variables de gestion de la vue et de la trackball
    OpenMesh::Vec3f  center_;
    float            radius_;

    GLdouble    projection_matrix_[16], modelview_matrix_[16];

    QPoint           last_point_2D_;
    OpenMesh::Vec3f  last_point_3D_;
    bool             last_point_ok_;


public slots:

signals:  // On ne les implémente pas, elles seront générées par MOC ;
		  // les paramètres seront passés aux slots connectés.

protected slots:
	void onTimeout();

protected:

    //events GL
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    /*!
     * \brief Dessine les points de contrôle déclarés préalablement
     * \param position
     */
    void dessinerPoints2Controle(unsigned position);

    /*!
     * \brief Dessine les courbes déclarées préalablement en forme pleine ou fil de fer
     * \param position
     */
    void dessinerCourbes(unsigned position);

    /*!
     * \brief Dessine le point mobile en fonction de u et v
     * \param m
     */
    void dessinerPointMobile(QMatrix4x4 m);

	void doProjection();
	void keyPressEvent(QKeyEvent *ev) override;
	void keyReleaseEvent(QKeyEvent *ev) override;
	void mousePressEvent(QMouseEvent *ev) override;
	void mouseReleaseEvent(QMouseEvent *ev) override;
	void mouseMoveEvent(QMouseEvent *ev) override;

private:
    /*!
     * \brief Angle de rotation de la vue autour de l'axe x
     */
    double m_angle_x = 0;

    /*!
     * \brief Angle de rotation de la vue autour de l'axe y
     */
    double m_angle_y = 0;

    QTimer *m_timer = nullptr;
    double m_ratio = 1;

    /*!
     * \brief Position du point mobile
     */
    double u = 0.0;

    /*!
     * \brief Position du point mobile
     */
    double v = 0.0;

    /*!
     * \brief Pas de discrétisation
     */
    double step = 0.5;
    bool fill = false;

	//RR matrices utiles
	QMatrix4x4 m_modelView;
    QMatrix4x4 m_projection;
    QMatrix4x4 m_model;

	QOpenGLShaderProgram *m_program;
    QOpenGLBuffer m_vbo;

    /*!
     * \brief Nombre de groupes de points de contrôle
     */
    unsigned nb_param_m;

    /*!
     * \brief Taille des groupes de points de contrôle
     */
    unsigned nb_param_n;
    float* colors;

    /*!
     * \brief Remplit le VBO avec les points tirés de la discrétisation d'une courbe 2D
     * \param courbes: Tableau des courbes à discrétiser
     * \return
     */
    QVector<GLfloat> preparationCourbe2D(QVector<CourbeParam*> courbes);

    /*!
     * \brief Remplit le VBO avec les points tirés de la discrétisation d'un carreau de Béziers
     * \param courbes: Tableau des courbes à discrétiser
     * \return
     */
    QVector<GLfloat> preparationBezier(QVector<CourbeParam*> courbes);

    QVector<CourbeParam*> formes;

	void makeGLObjects();
    void tearGLObjects();
};


#endif // MYOPENGLWIDGET_H
