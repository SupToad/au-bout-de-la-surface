//R. Raffin, M1 Informatique, "Surfaces 3D"
//tiré de CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "myopenglwidget.h"
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>

#include <iostream>

#include "segment.h"

static const QString vertexShaderFile   = ":/basic.vsh";
static const QString fragmentShaderFile = ":/basic.fsh";

myOpenGLWidget::myOpenGLWidget(QWidget *parent) :
	QOpenGLWidget(parent)
{
	qDebug() << "init myOpenGLWidget" ;

	QSurfaceFormat sf;
	sf.setDepthBufferSize(24);
	sf.setSamples(16);  // nb de sample par pixels : suréchantillonnage por l'antialiasing, en décalant à chaque fois le sommet
						// cf https://www.khronos.org/opengl/wiki/Multisampling et https://stackoverflow.com/a/14474260
	setFormat(sf);

	setEnabled(true);  // événements clavier et souris
	setFocusPolicy(Qt::StrongFocus); // accepte focus
	setFocus();                      // donne le focus

	m_timer = new QTimer(this);
	m_timer->setInterval(50);  // msec
	connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));

    nb_param_m = 0;
    nb_param_n = 0;
    colors = nullptr;
    triToDraw = 0;
    linesToDraw = 0;
    pointsToDraw = 0;
}

myOpenGLWidget::~myOpenGLWidget()
{
	qDebug() << "destroy GLArea";

    delete m_timer;
    for (unsigned i = 0 ; i < formes.size() ; ++i)
    {
        delete formes.data()[i];
        formes.remove(i);
    }

    vert_data.clear();
    formes.clear();

    delete [] colors;

	// Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
	// dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
	makeCurrent();
	tearGLObjects();
    doneCurrent();
}


void myOpenGLWidget::initializeGL()
{
	qDebug() << __FUNCTION__ ;
	initializeOpenGLFunctions();
	glEnable(GL_DEPTH_TEST);

	makeGLObjects();

	//shaders
	m_program = new QOpenGLShaderProgram(this);
	m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
	m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);

	if (! m_program->link()) {  // édition de lien des shaders dans le shader program
		qWarning("Failed to compile and link shader program:");
		qWarning() << m_program->log();
	}
}

void myOpenGLWidget::doProjection()
{
	//m_mod.setToIdentity();
	//modelMatrix.ortho( -aratio, aratio, -1.0f, 1.0f, -1.0f, 1.0f );
}

void myOpenGLWidget::generateColors(unsigned nb_points)
{
    srand(time(NULL));
    colors = new float[nb_points*3];
   for (unsigned i = 0 ; i < nb_points*3 ; ++i)
           colors[i] = 0.6f; //float(rand()%255)/255;
}

QVector<GLfloat> myOpenGLWidget::preparationBezier(QVector<CourbeParam*> courbes)
{
    qDebug() << __FUNCTION__ ;
    Discretisation d;
    // Récupère le nuage de points ordonné pour l'affichage
    QVector<Point> points = d.discretiserSurface(courbes,step,step);
    QVector<GLfloat> data;

    // Récupère les couleurs
    generateColors(points.size());

    // Charge les points avec leur couleur
    for (int i = 0 ; i < points.size() ; i++)
    {
        vert_data.append(points.data()[i].getX());
        vert_data.append(points.data()[i].getY());
        vert_data.append(points.data()[i].getZ());
        vert_data.append(colors[i*3]);
        vert_data.append(colors[i*3+1]);
        vert_data.append(colors[i*3+2]);
    }

    // Charge les points de contrôle
    for (unsigned j = 0 ; j < getNbParamN() ; ++j)
    {
        // D'abord les points eux même
        for (unsigned i = 0 ; i < getNbParamM() ; ++i)
        {
            unsigned p = i+j*getNbParamM();
            vert_data.append(courbes.data()[0]->getN(p).getX());
            vert_data.append(courbes.data()[0]->getN(p).getY());
            vert_data.append(courbes.data()[0]->getN(p).getZ());
            vert_data.append(0);
            vert_data.append(0);
            vert_data.append(0.7f);
        }

        // Puis les arêtes entre groupes de points de taille getNbParamM()
        if (j+1 < getNbParamN())
            for (unsigned i = 0 ; i < getNbParamM() ; ++i)
            {
                unsigned p = i+j*getNbParamM();
                vert_data.append(courbes.data()[0]->getN(p).getX());
                vert_data.append(courbes.data()[0]->getN(p).getY());
                vert_data.append(courbes.data()[0]->getN(p).getZ());
                vert_data.append(0);
                vert_data.append(0);
                vert_data.append(0.7f);

                unsigned p2 = p+getNbParamM();
                vert_data.append(courbes.data()[0]->getN(p2).getX());
                vert_data.append(courbes.data()[0]->getN(p2).getY());
                vert_data.append(courbes.data()[0]->getN(p2).getZ());
                vert_data.append(0);
                vert_data.append(0);
                vert_data.append(0.7f);
            }
    }

    return data;

}

QVector<GLfloat> myOpenGLWidget::preparationCourbe2D(QVector<CourbeParam*> courbes)
{
    Discretisation d;
    QVector<Point> points = d.avancee2Front(courbes,0.2f,0,0.5);
    QVector<GLfloat> data;

    generateColors(points.size());

    qDebug() << __FUNCTION__ ;
    for (unsigned i = 0 ; i < points.size() ; i++)
    {
        vert_data.append(points.data()[i].getX());
        vert_data.append(points.data()[i].getY());
        vert_data.append(points.data()[i].getZ());
        vert_data.append(colors[i*3]);
        vert_data.append(colors[i*3+1]);
        vert_data.append(colors[i*3+2]);
    }

    return data;
}

void myOpenGLWidget::makeGLObjects()
{
    qDebug() << __FUNCTION__ ;

    // Détruit les formes déjà construites
    formes.clear();
    vert_data.clear();
    if (colors != nullptr) delete [] colors;

    // Paramètre des points de contrôle
    nb_param_m = 4;
    nb_param_n = 4;

    // Points de contrôle par défaut

    // Point control[8] = {Point(-1,0,0), Point(-0.66,1,0), Point(0.66,1,0), Point(1,0,0),Point(0,0,-1), Point(0,1,-0.66), Point(0,1,0.66), Point(0,0,1)};
    // Point control [8] = {Point(-1,0,0), Point(-0.66,1,0), Point(0.66,1,0), Point(1,0,0),
    //                      Point(-1,0,1), Point(-0.66,1,1), Point(0.66,1,1), Point(1,0,1)};
    Point control [16] =
    { Point(-1.0, -0.25, +1.5),Point(-0.3, -0.5, +1.5),Point(+0.3, -0.5, +1.5),Point(+1.0, -0.25, +1.5),
      Point(-1.0, -0.5, +0.5), Point(-0.3, +1.5, +0.5),Point(+0.3, +1.5, +0.5),Point(+1.0, -0.5, +0.5),
      Point(-1.0, -0.5, -0.5), Point(-0.3, +0.5, -0.5),Point(+0.3, +0.5, -0.5),Point(+1.0, -0.5, -0.5),
      Point(-1.0, -0.25, -1.5),Point(-0.3, -0.5, -1.5),Point(+0.3, -0.5, -1.5),Point(+1.0, -0.25, -1.5)};

    formes.append(new CarreauBezier(nb_param_m,nb_param_n,control));

    // Récupère l'ensemble des points
    preparationBezier(formes);

    // Construit manuellement le point mobile
    vert_data.append(0);
    vert_data.append(0);
    vert_data.append(0);

    vert_data.append(0);
    vert_data.append(1);
    vert_data.append(0);

    // Configure le vbo
    m_vbo.create();
    m_vbo.bind();

    m_vbo.allocate(vert_data.data(), vert_data.count() * sizeof(GLfloat));

    qDebug() << __FUNCTION__ ;
}


void myOpenGLWidget::tearGLObjects()
{
    m_vbo.destroy();
}


void myOpenGLWidget::resizeGL(int w, int h)
{
	qDebug() << __FUNCTION__ << w << h;

	//C'est fait par défaut
	glViewport(0, 0, w, h);

	m_ratio = (double) w / h;
	//doProjection();
}

void myOpenGLWidget::paintGL()
{
	qDebug() << __FUNCTION__ ;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_program->bind(); // active le shader program

	/// Ajout RR pour gérer les 3 matrices utiles
	/// à mettre dans doProjection() pour clarifier
	/// -----------------------------------------
		m_modelView.setToIdentity();
		m_modelView.lookAt(QVector3D(0.0f, 0.0f, 3.0f),    // Camera Position
						 QVector3D(0.0f, 0.0f, 0.0f),    // Point camera looks towards
						 QVector3D(0.0f, 1.0f, 0.0f));   // Up vector

		m_projection.setToIdentity ();
		m_projection.perspective(70.0, width() / height(), 0.1, 100.0); //ou m_ratio

		// Rotation de la scène pour l'animation
        m_modelView.rotate(m_angle_x, 1, 0, 0);
        m_modelView.rotate(m_angle_y, 0, 1, 0);

		QMatrix4x4 m = m_projection * m_modelView * m_model;
	///----------------------------

	m_program->setUniformValue("matrix", m);


    m_program->setAttributeBuffer("posAttr", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
    m_program->setAttributeBuffer("colAttr", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
    m_program->enableAttributeArray("posAttr");
    m_program->enableAttributeArray("colAttr");

    dessinerPointMobile(m);

    m_program->setUniformValue("matrix", m);

    // Nombre de points de la surface
    // Pour séparer celle-ci des points de contrôle
    unsigned taille_pts = (vert_data.size()/6)-(getNbParam()-1)*3+getNbParamM();

    glPointSize (5.0f);
    glLineWidth(2.0f);

    dessinerCourbes(taille_pts);
    dessinerPoints2Controle(taille_pts);

    m_program->disableAttributeArray("posAttr");
    m_program->disableAttributeArray("colAttr");

	m_program->release();
}

void myOpenGLWidget::dessinerPointMobile(QMatrix4x4 m)
{
    glPointSize (15.0f);

    // Translate le point en fonction des paramètres u et v
    QMatrix4x4 m2 = m;
    float paramValues[2];
    paramValues[0] = getU();
    paramValues[1] = getV();
    Parametre param ( 2 , paramValues );
    m2.translate(formes.first()->getValue(param).getX(),
                 formes.first()->getValue(param).getY(),
                 formes.first()->getValue(param).getZ());
    m_program->setUniformValue("matrix", m2);

    glDrawArrays(GL_POINTS, vert_data.size()/6-1,1);
}

void myOpenGLWidget::dessinerCourbes(unsigned int position)
{
    // Forme fil de fer
    if (!fill )
        for (int i = 0 ; i < position ; i += 4)
        {
            glDrawArrays( GL_LINE_LOOP , i , 3 );
            glDrawArrays( GL_LINE_LOOP , i + 1 , 3 );
        }
    // Forme pleine
    if ( fill )
        for (int i = 0 ; i < position ; i += 4)
        {
            glDrawArrays( GL_TRIANGLES , i , 3 );
            glDrawArrays( GL_TRIANGLES , i + 1 , 3 );
        }
}

void myOpenGLWidget::dessinerPoints2Controle(unsigned int position)
{
    glLineWidth(7.0f);
    glPointSize (15.0f);
    for (unsigned j = 0 ; j < getNbParamN()*3 ; j+=3)
    {
        // Relie les points en séparant par groupe de getNbParamM()
        for (unsigned i = 0 ; i < getNbParamM()-1 ; ++i)
        {
            glDrawArrays(GL_POINTS, j*getNbParamM()+i+position, 1);
            glDrawArrays(GL_LINES, j*getNbParamM()+i+position, 2);
        }
        glDrawArrays(GL_POINTS, j*getNbParamM()+getNbParamM()-1+position, 1);

        // Relie les points séparés
        for (unsigned i = 0 ; i < getNbParamM()*2 ; i+=2)
        {
            glDrawArrays(GL_LINES, j*getNbParamM()+i+getNbParamM()+position, 2);
        }
    }
}

void myOpenGLWidget::keyPressEvent(QKeyEvent *ev)
{
	qDebug() << __FUNCTION__ << ev->text();

    switch(ev->key()) {
        default:
            break;
	}
}

void myOpenGLWidget::keyReleaseEvent(QKeyEvent *ev)
{
	qDebug() << __FUNCTION__ << ev->text();
}

void myOpenGLWidget::mousePressEvent(QMouseEvent *ev)
{
	qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void myOpenGLWidget::mouseReleaseEvent(QMouseEvent *ev)
{
	qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void myOpenGLWidget::mouseMoveEvent(QMouseEvent *ev)
{
	qDebug() << __FUNCTION__ << ev->x() << ev->y();
}

void myOpenGLWidget::onTimeout()
{
	qDebug() << __FUNCTION__ ;

	update();
}

double myOpenGLWidget::getU () const { return this->u; }
double myOpenGLWidget::getV () const { return this->v; }
double myOpenGLWidget::getStep () const { return this->step; }
void myOpenGLWidget::setU ( double u ) { this->u = u; update(); }
void myOpenGLWidget::setV ( double v ) { this->v = v; update(); }
void myOpenGLWidget::setStep ( double step ) { this->step = step; makeGLObjects() ; update(); }

bool myOpenGLWidget::getFill() { return this->fill; }
void myOpenGLWidget::switchFill() { this->fill = !this->fill; update(); }

double myOpenGLWidget::getAngle_y () { return this->m_angle_y; }
void myOpenGLWidget::setAngle_y ( double angle_y ) { this->m_angle_y = angle_y; update(); }

double myOpenGLWidget::getAngle_x () { return this->m_angle_x; }
void myOpenGLWidget::setAngle_x ( double angle_x ) { this->m_angle_x = angle_x; update(); }



