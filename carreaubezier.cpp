#include "carreaubezier.h"

CarreauBezier::CarreauBezier(unsigned m, unsigned n, Point* _points)
    :Bezier(m*n,_points), size(n)
{}

CarreauBezier::CarreauBezier(unsigned m, unsigned n, QVector<Point> _points)
    :Bezier (m*n,_points), size(n)
{}

CarreauBezier::~CarreauBezier()
{
}

Point CarreauBezier::getValue(Parametre param) const
{
    Point p;
    uint m = getSize()/size;
    uint n = size;
    for (unsigned i = 0 ; i < n ; ++i)
        for (unsigned j = 0 ; j < m ; ++j)
        {
            p.setX(p.getX()+bernstein(param.getParamf().first(),i,n-1)*bernstein(param.getParamf().last(),j,m-1)*getN(i*m+j).getX());
            p.setY(p.getY()+bernstein(param.getParamf().first(),i,n-1)*bernstein(param.getParamf().last(),j,m-1)*getN(i*m+j).getY());
            p.setZ(p.getZ()+bernstein(param.getParamf().first(),i,n-1)*bernstein(param.getParamf().last(),j,m-1)*getN(i*m+j).getZ());
        }
    return p;
}
