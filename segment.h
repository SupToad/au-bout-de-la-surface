/* R. Raffin
 * M1 Informatique, Aix-Marseille Université
 * définition d'une classe Segment
 * Comme pour Point, il faudrait mieux utiliser un std::Vector pour stocker les 2 Poins du segment, cela évite de manipuler des pointeurs (et d'en faire allocation et libération.
 * Un Segment n'a par contre que 2 élements Point, le tbaleau est donc de taille fixe
 * Pour aller plus loin, il faut pouvoir évaluer un point sur le segment en fonction d'un paramètre.
 * Ce paramètre doit appartenir à l'intervalle [0,1] et il faudrait donc stocker/vérifier cet intervalle.
 */

#ifndef SEGMENT_H
#define SEGMENT_H

#include <QVector>
#include <QOpenGLWidget>

#include "point.h"
#include "courbeparam.h"

/*!
 * \brief Classe représentant un segment, sorte de courbe paramétrique
 */
class Segment : public CourbeParam
{
public:
    /*!
     * \brief Constructeur de la classe Segment à partir de deux points
     * \param begin: Premier point du segment
     * \param end: Second point du segment
     */
    Segment(Point begin, Point end);

    /*!
     * \brief Constructeur par défaut de la classe segment
     */
    Segment();
	~Segment();

    /*!
     * \brief Constructeur par recopie de la classe Segment
     */
	Segment(const Segment&);

    /*!
     * \brief Surcharge de l'opérateur de comparaison
     * \return Le segment avec sa nouvelle valeur
     */
    Segment& operator= (const Segment &);

    /*!
     * \brief Setter du premier point du segment
     */
    void setStart(const Point&);

    /*!
     * \brief Setter du second point du segment
     */
    void setEnd(const Point&);

    /*!
     * \brief getter du premier point du segment
     * \return
     */
    Point getStart() const;

    /*!
     * \brief getter du second point du segment
     * \return
     */
    Point getEnd() const;

    /*!
     * \brief Calcule le vecteur entre deux points quelquonques du segment
     * \param param1: Paramètre pour calcul du point 1
     * \param param2: Paramètre pour calcul du point 2
     * \return Point représentant le vecteur calculé
     */
    Point getVector(float param1, float param2) const;

    /*!
     * \brief Retourne la taille du segment
     * \return
     */
    float length() const;

    /*!
     * \brief Retourne les coordonnées du point issus de l'équation du segment avec
     * le paramètre donné
     * \param param: Paramètre à passer à l'équation
     * \return Point calculé
     */
    Point getValue(Parametre param) const;

    /*!
     * \brief Retourne les coordonnées du point issus de l'équation du segment avec
     * le paramètre donné
     * \param param: Paramètre à passer à l'équation
     * \return Point calculé
     */
    Point getValue(float param) const;

    /*!
     * \brief Surcharge de l'opérateur de flux
     * \return Un OutputStream
     */
    friend std::ostream& operator<<(std::ostream&, const Segment&);
};

#endif // SEGMENT_H
