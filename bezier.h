#ifndef BEZIER_H
#define BEZIER_H

#include <QVector>
#include <QOpenGLWidget>

#include "point.h"
#include "courbeparam.h"

/*!
 * \brief Classe représentant une courbe de Béziers. Hérite de la classe CourbeParam
 */
class Bezier : public CourbeParam
{
public:
    /*!
     * \brief Constructeur de la classe Béziers à partir d'un tableau C de Point
     * \param _size: Taille du tableau de points
     * \param Tableau de points
     */
    Bezier(unsigned _size, Point* control);

    /*!
     * \brief Constructeur de la classe Béziers à partir d'un QVector de Point
     * \param _size: Taille du tableau de points
     * \param Tableau de pointsl
     */
    Bezier(unsigned _size, QVector<Point> control);

    /*!
    * \brief Destructeur par défaut de la classe Béziers
    */
    ~Bezier();

    /*!
     * \brief Calcule les coordonnées d'une Point sur la courbe de Béziers à partir
     * d'une instance de la classe Paramètre, contenant plusieurs paramètres or-
     * donnés à utiliser dans le polynome de Bernstein de la courbe
     * \param param: Instance de la classe Param
     * \return Retourne le Point calculé.
     */
    Point getValue(Parametre param) const;

    /*!
     * \brief Calcule les coordonnées d'une Point sur la courbe de Béziers à partir
     * d'un unique paramètre à utiliser dans le polynome de Bernstein de la courbe
     * \param param: paramètre à utiliser
     * \return Retourne le Point calculé.
     */
    Point getValue(float param) const;

    /*!
     * \brief Calcule le résultat d'un polynôme de Bernstein d'une courbe de Béziers
     * en fonction de son rang, son degré et d'un paramètre t
     * \param t: Paramètre du polynôme de Bernstein
     * \param rang: Rang du point de contrôle à calculer dans le polynôme
     * \param degres: Degré du polynôme (nombre de points de contrôle - 1)
     * \return Le résultat du calcul du polynôme de Bernstein
     */
    float bernstein(float t, unsigned rang, unsigned degres) const;
};

#endif // BEZIER_H
