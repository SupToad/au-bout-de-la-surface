#ifndef COURBEPARAM_H
#define COURBEPARAM_H

#include <QVector>
#include <QOpenGLWidget>
#include <cmath>
#include "point.h"
#include "parametre.h"


/*!
 * \brief Classe virtuelle représentant une courbe paramétrique. Elle est implémentée
 * dans les classes Bezier et Segment
 */
class CourbeParam
{
protected:
    QVector<Point> pointList;
    float* vertices;
public:
    /*!
     * \brief Constructeur par défaut de la classe CourbeParam
     */
    CourbeParam();

    /*!
     * \brief Constructeur prenant en paramètre le nombre de points de contrôle de
     * la courbe paramétrique
     * \param _size: Nombre de points de contrôle de la courbe paramètrique
     */
    CourbeParam(unsigned _size);

    virtual ~CourbeParam();

    /*!
     * \brief Getter du point de contrôle à l'index souhaité dans le tableau des points
     * de contrôle
     * \return Le point de contrôle souhaité
     */
    Point getN(unsigned) const;

    /*!
     * \brief Setter du point de contrôle à l'index souhaité dans le tableau des points
     * de contrôle
     */
    void setN(unsigned, const Point&);

    /*!
     * \brief Calcule les coordonnées d'une Point sur la courbe de Béziers à partir
     * d'une instance de la classe Paramètre, contenant plusieurs paramètres or-
     * donnés à utiliser dans le polynome de Bernstein de la courbe
     * \param param: Instance de la classe Param
     * \return Retourne le Point calculé.
     */
    virtual Point getValue(Parametre param) const {return Point();}

    /*!
     * \brief Calcule les coordonnées d'une Point sur la courbe de Béziers à partir
     * d'un unique paramètre à utiliser dans le polynome de Bernstein de la courbe
     * \param param: paramètre à utiliser
     * \return Retourne le Point calculé.
     */
    virtual Point getValue(float param) const {return Point();}

    /*!
     * \brief Retourne le nombre de points de contrôle
     */
    virtual inline unsigned getSize() const {return pointList.size();}

    /*!
     * \brief Calcule les coordonnées du vecteur entre deux points et stocke ses coordonnées
     * dans un Point
     * \param param1: Premier float à passer à la fonction getValue(float param)
     * \param param1: Second float à passer à la fonction getValue(float param)
     * \return Le point correspondant au vecteur calculé
     */
    Point getVector(float param1, float param2) const;

    /*!
     * \brief Calcule les coordonnées du vecteur entre deux points et stocke ses coordonnées
     * dans un Point
     * \param param1: Premier float à passer à la fonction getValue(Parametre param)
     * \param param1: Second float à passer à la fonction getValue(Parametre param)
     * \return Le point correspondant au vecteur calculé
     */
    Point getVector(Parametre param1, Parametre param2) const;
};

#endif // COURBEPARAM_H
