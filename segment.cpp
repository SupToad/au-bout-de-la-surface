#include <cmath>

#include "segment.h"

Segment::Segment()
{
    vertices = new float[6];
}

Segment::Segment(Point begin, Point end)
{
    vertices = new float[getSize()*3];

    setStart(begin);
    setEnd(end);
}

Segment::~Segment()
{
}

Segment::Segment(const Segment & s)
{
    vertices = new float[6];
    for (unsigned i=0; i<2; ++i)
        pointList.append(s.getN(i));

}

void Segment::setStart(const Point & p)
{
    setN(0,p);
}


void Segment::setEnd(const Point & p)
{
    setN(getSize() - 1,p);
}

Point Segment::getStart() const
{
    return getN(0);
}


Point Segment::getEnd() const
{
    return getN(getSize()-1);
}

Point Segment::getValue(float param) const
{
    return getStart()+param*(getEnd()-getStart());
}

Point Segment::getValue(Parametre param) const
{
    return getStart()+param.getParamf().first()*(getEnd()-getStart());
}


float Segment::length() const
{
    void getData(QVector<GLfloat>* data);
    float res=0.0f;

    for (unsigned i=0; i<3; ++i)
        res += pow((pointList.at(1)).getN(i) - (pointList.at(0)).getN(i), 2.0f);

    return sqrt(res);
}

std::ostream& operator<<(std::ostream& out, const Segment& s)
{
    return out << s.getStart() << " -- " << s.getEnd();
}

Segment& Segment::operator= (const Segment &s)
{
    for (unsigned i=0; i<2; ++i)
        setN(i,s.getN(i));

    return *this;
}
