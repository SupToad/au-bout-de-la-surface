#ifndef PARAMETRE_H
#define PARAMETRE_H

#include<QVector>
#include "point.h"

/*!
 * \brief Classe représentant un paramètre à passer à une courbe paramétrique
 */
class Parametre
{
private:
    /*!
     * \brief Liste des paramètres float
     */
    QVector<float> paramf;

    /*!
     * \brief Liste des points formant les paramètres
     */
    QVector<QVector<Point>> paramp;
public:
    /*!
     * \brief Constructeur par défaut de la classe Paramètre
     */
    Parametre();

    /*!
     * \brief Constructeur alternatif de la classe paramètre
     * \param nb: Nombre de paramètres
     * \param param: tableau de paramètres
     */
    Parametre(unsigned nb, float* param);

    /*!
     * \brief Constructeur alternatif de la classe paramètre
     * \param nb: Nombre de paramètres
     * \param param: tableau de paramètres
     */
    Parametre(unsigned nb, QVector<Point>* param);

    /*!
     * \brief getter de la variable paramf
     * \return
     */
    inline QVector<float> getParamf() const {return paramf;}

    /*!
     * \brief getter de la variable paramp
     * \return
     */
    inline QVector<QVector<Point>> getParamp() const {return paramp;}

    /*!
     * \brief Ajoute un paramètre float
     * \param param: Paramètre à ajouter
     */
    void addParamf(const float param);

    /*!
     * \brief Ajoute un paramètre Point
     * \param param: Paramètre à ajouter
     * \param to: A quel tableau ajouter le point
     */
    void addParamp(const Point param, unsigned to);

    /*!
     * \brief Enlève un paramètre
     * \param i: Index du paramètre à enlever
     */
    void removeParamf(const unsigned i) {paramf.remove(i);}

    /*!
     * \brief Enlève le dernier paramètre
     */
    void removeParamf() {paramf.remove(paramf.size()-1);}
};

#endif // PARAMETRE_H
