#ifndef DISCRETISATION_H
#define DISCRETISATION_H

#include "segment.h"
#include "parametre.h"


/*!
 * \brief Cette classe contient diverses fonctions de discrétisation appliquées à des
 * ensembles de courbes paramétriques ou de segments
 */
class Discretisation
{
public:
    Discretisation();

    /*!
     * \brief Discretisation uniforme d'une liste de segments selon un paramètre p
     * \param s: Liste des segments à discrétiser
     * \param p: Pas de discrétisation
     * \return Le tableau des points issus de la discrétisation
     */
    QVector<Point> discretiserSegment(QVector<Segment> s, Parametre p);

    /*!
     * \brief Discrétisation uniforme d'une surface paramètrique (formées de plusieurs
     * courbes de Béziers) selon deux paramètres u et v
     * \param cur_list: Liste des courbes à discrétiser
     * \param delta_u: Pas de discrétisation "parallèle" à la forme
     * \param delta_v: Pas de discrétisation "perpendiculaire" à la forme
     * \return Le tableau des points issus de la discrétisation
     */
    QVector<Point> discretiserSurface (QVector<CourbeParam*> cur_list, float delta_u, float delta_v);

    /*!
     * \brief Discrétisation d'une liste de courbes paramétriques par avancée de front
     * \param cur_list: Liste des courbes à discrétiser
     * \param del_ta: pas de discrétisation initial
     * \param seuil_bas: Seuil au-delà duquel deux points successifs sont trop ecartés
     * \param seuil_haut: Seuil en-deça duquel deux points successifs sont trop rapprochés
     * \return Le tableau des points issus de la discrétisation
     */
    QVector<Point> avancee2Front (QVector<CourbeParam*> cur_list, float del_ta, double seuil_bas, double seuil_haut);
};

#endif // DISCRETISATION_H
