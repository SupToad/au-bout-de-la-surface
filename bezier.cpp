#include "bezier.h"

unsigned fact(unsigned f)
{
    unsigned res = 1;
    for (unsigned i = 1 ; i <= f ; i++)
        res *= i;
    return res;
}

Bezier::Bezier(unsigned _size, Point* control)
{
    for (unsigned i = 0 ; i < _size ; ++i)
        setN(i,control[i]);
}

Bezier::Bezier(unsigned _size, QVector<Point> control)
{
    for (unsigned i = 0 ; i < _size ; ++i)
        setN(i, control.data()[i]);
}

Bezier::~Bezier()
{

}

Point Bezier::getValue(Parametre param) const
{
    Point p;
    for (unsigned i = 0 ; i < getSize() ; ++i)
    {
        p.setX(p.getX()+bernstein(param.getParamf().first(),i,getSize()-1)*getN(i).getX());
        p.setY(p.getY()+bernstein(param.getParamf().first(),i,getSize()-1)*getN(i).getY());
        p.setZ(p.getZ()+bernstein(param.getParamf().first(),i,getSize()-1)*getN(i).getZ());
    }
    return p;
}

Point Bezier::getValue(float param) const
{
    Point p;
    for (unsigned i = 0 ; i < getSize() ; ++i)
    {
        p.setX(p.getX()+bernstein(param,i,getSize()-1)*getN(i).getX());
        p.setY(p.getY()+bernstein(param,i,getSize()-1)*getN(i).getY());
        p.setZ(p.getZ()+bernstein(param,i,getSize()-1)*getN(i).getZ());
    }
    return p;
}

float Bezier::bernstein(float t, unsigned int rang, unsigned int degres) const
{
    float res = fact(degres)/(fact(rang)*fact(degres-rang));
    res *= float(pow(t, rang));
    res *= float(pow(1-t, degres-rang));
    return res;
}
