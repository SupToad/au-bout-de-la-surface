/* R. Raffin
 * M1 Informatique, Aix-Marseille Université
 * Fenêtre principale
 * Au cas où, l'UI contient une barre de menu, une barre de status, une barre d'outils (cf QMainWindow).
 * Une zone est laissée libre à droite du Widget OpenGL pour mettre de futurs contrôles ou informations.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);
    drawArea->setStep(0.02);
}

double MainWindow::getUValue() {
    return this->u_value->value();
}

double MainWindow::getVValue() {
    return this->v_value->value();
}

double MainWindow::getStepValue() {
    return this->step_value->value();
}


void MainWindow::on_u_value_valueChanged(double arg1)
{
    u_value->setValue(arg1);
    drawArea->setU(arg1);
}

void MainWindow::on_v_value_valueChanged(double arg1)
{
    v_value->setValue(arg1);
    drawArea->setV(arg1);
}

void MainWindow::on_step_value_valueChanged(double arg1)
{
    step_value->setValue(arg1);
    drawArea->setStep(1/arg1);
}

void MainWindow::on_shape_clicked()
{
    drawArea->switchFill();
}

void MainWindow::on_pushButton_chargement_clicked()
{
    int size = drawArea->getForme(0)->getSize();
    // Charge les sommets
    MyMesh::VertexHandle vhandle[size];
    for (int i = 0; i < size; i++)
    {
        Point pt = drawArea->getForme(0)->getN(i);
        vhandle[i] = mesh.add_vertex(MyMesh::Point(pt.getX(), pt.getY(),  pt.getZ()));
    }
    std::vector<MyMesh::VertexHandle>  face_vhandles;

    // Dessine les faces de devant (en ordre ccw)
    for (unsigned j = 0 ; j+2 < drawArea->getNbParamM() ; ++j)
    {
        face_vhandles.clear();
        face_vhandles.push_back(vhandle[drawArea->getNbParamM()-1-j]);
        face_vhandles.push_back(vhandle[drawArea->getNbParamM()-2-j]);
        face_vhandles.push_back(vhandle[0]);
        mesh.add_face(face_vhandles);
    }

    // Dessine les faces de côté (en ordre ccw)
    for (unsigned i = 1 ; i < drawArea->getNbParamN() ; ++i)
    {
        for (unsigned j = 1 ; j < drawArea->getNbParamM() ; ++j)
        {
            face_vhandles.clear();
            face_vhandles.push_back(vhandle[j + i*drawArea->getNbParamM()]);
            face_vhandles.push_back(vhandle[j-1 + i*drawArea->getNbParamM()]);
            face_vhandles.push_back(vhandle[j-1 + (i-1)*drawArea->getNbParamM()]);
            face_vhandles.push_back(vhandle[j + (i-1)*drawArea->getNbParamM()]);
            mesh.add_face(face_vhandles);
        }
        // Ferme la figure par le bas
        face_vhandles.clear();
        face_vhandles.push_back(vhandle[i*drawArea->getNbParamM() + drawArea->getNbParamM() -1]);
        face_vhandles.push_back(vhandle[(i-1)*drawArea->getNbParamM() + drawArea->getNbParamM()-1]);
        face_vhandles.push_back(vhandle[(i-1)*drawArea->getNbParamM()]);
        face_vhandles.push_back(vhandle[i*drawArea->getNbParamM()]);
        mesh.add_face(face_vhandles);
    }

    // Dessine les faces de derrière (ordre cw)
    for (unsigned j = 0 ; j+2 < drawArea->getNbParamM() ; ++j)
    {
        face_vhandles.clear();
        face_vhandles.push_back(vhandle[drawArea->getNbParamM()*(drawArea->getNbParamN()-1)]);
        face_vhandles.push_back(vhandle[drawArea->getNbParamM()*(drawArea->getNbParamN()-1)+j+1]);
        face_vhandles.push_back(vhandle[drawArea->getNbParamM()*(drawArea->getNbParamN()-1)+j+2]);
        mesh.add_face(face_vhandles);
    }

    // write mesh to output.obj
    try
    {
      if ( !OpenMesh::IO::write_mesh(mesh, "output.obj") )
      {
        std::cerr << "Cannot write mesh to file 'output.obj'" << std::endl;
        exit(1);
      }
    }
    catch( std::exception& x )
    {
      std::cerr << x.what() << std::endl;
      exit(1);
    }
}

void MainWindow::on_x_angle_valueChanged(int value)
{
    drawArea->setAngle_x((double) value);
}

void MainWindow::on_y_angle_valueChanged(int value)
{
    drawArea->setAngle_y((double) value);
}
